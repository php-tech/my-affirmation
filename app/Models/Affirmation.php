<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Affirmation extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id', 'category_id', 'binaural_beat_id',
        'background_audio_id', 'name',
        'recorded_audio', 'mix_audio',
        'recorded_transcription', 'images', 'effect_type',
        'is_transcription_display', 'step'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['modification_date', 'audio_url', 'mixed_audio_url', 'images_url'];

    /**
     * Get the formatted modification date attribute.
     *
     * @return string
     */
    public function getModificationDateAttribute()
    {
        return Carbon::parse($this->attributes['updated_at'])->format('d-M-Y | h:i');
    }

    /**
     * Get the URL of the audio file attribute.
     *
     * @return string|null
     */
    public function getAudioUrlAttribute()
    {
        return $this->attributes['recorded_audio'] ? asset('storage/audio_file/' . $this->attributes['recorded_audio']) : null;
    }

    /**
     * Get the URL of the Mixed Audio file attribute.
     *
     * @return string|null
     */
    public function getMixedAudioUrlAttribute()
    {
        return $this->attributes['mix_audio'] ? asset('storage/mix_audio_file/' . $this->attributes['mix_audio']) : null;
    }

    /**
     * Get the URLs of the images attribute.
     *
     * @return array|null
     */
    public function getImagesUrlAttribute()
    {
        $urls = null;
        if ($this->attributes['images']) {
            foreach (explode(',', $this->attributes['images']) as $image) {
                $urls[] = $this->attributes['images'] ? asset('storage/affirmation/' . $this->attributes['id'] . '/' . $image) : null;
            }
        }
        return $urls;
    }
}
