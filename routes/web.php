<?php

use App\Http\Controllers\Frontend\AffirmationController;
use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Frontend\LoginController;
use App\Http\Controllers\Frontend\ProfileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

/** Admin Routes */
Route::get('welcome', function () {
    return view('welcome');
});

/** Front Routes */
Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/login', [LoginController::class, 'login'])->name('customer.login');
Route::post('/login', [LoginController::class, 'loginSubmit'])->name('customer.login');
Route::post('/logout', [LoginController::class, 'logout'])->name('customer.logout');

Route::middleware('user')->group(function () {

    /** Change Password */
    Route::get('/change-password', [ProfileController::class, 'changePassword'])->name('change.password');
    Route::post('/change-password', [ProfileController::class, 'submitChangePassword'])->name('change.password.submit');

    /** Affirmation Management */
    Route::get('/affirmation', [AffirmationController::class, 'index'])->name('affirmation');
    Route::get('/affirmation/create/{id?}', [AffirmationController::class, 'createOrEdit'])->name('affirmation.create');
    Route::post('/affirmation/store', [AffirmationController::class, 'store'])->name('affirmation.store');
});
