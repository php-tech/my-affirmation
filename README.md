
# My Affirmation

### Technology Used
 
- PHP v8.1
- Laravel v10
- Inertia JS
- React JS
- MySql
- PSR 4 Standard ( As client required )

### Example Modules

- My Affirmation 
    - Create four steps
    - Step 1: Record my voice with transcription
    - Step 2: Sound mixing (Merge the two diffrent 
            audio sound with recorded sound)
    - Step 3: Upload multiple images and select 
            the effect type (Kaleidoscop / Zoom in zoom out)
    - Step 4: Play Audio / Review Audio