import Logo from '../../images/logo.svg';

export default function ApplicationLogo(props) {
    return (

        <>
            <img src={Logo} alt='logo' className='w-6/12 md:w-9/12 object-contain' />
        </>
    );
}
